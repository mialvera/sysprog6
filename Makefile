CC=gcc
DEPS = mask.h
OBJ = main.o mask.o 

all: permisos

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< 

permisos: $(OBJ)
	gcc -o $@ $^ 

clean:
	rm -f permisos *.o
