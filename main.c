#include "mask.h"
#include <stdio.h>

static int mask_owner, mask_group, mask_other;

int main() {

	pedirPermisos(&mask_owner, "owner");

	pedirPermisos(&mask_group, "group");

	pedirPermisos(&mask_other, "other");

	printf("\nMáscara de permisos: %d%d%d\n", mask_owner, mask_group, mask_other);

}
