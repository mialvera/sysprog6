#include "mask.h"
#include <stdio.h>
/*
El parámetro mascara es la máscara que se debe mantener para cada nivel de acceso y debe de ser enviado por referencia. El parámetro permiso es un char con los valores r, w, o x y set es un boolean con valores de  0 o 1. Los valores de 0 y 1 deben de estar definidos con un #define como SET y UNSET. 
*/
void setPermiso(int *mascara, char permiso, int set) {
	switch(permiso) {
		case 'r':
			*mascara = *mascara | (set << 2);
			break;
		case 'w':
			*mascara = *mascara | (set << 1);
			break;
		case 'x':
			*mascara = *mascara | set;
			break;
	}
}

void pedirPermisos(int *mascara, char *tipoUsuario) {

	pedirPermiso(mascara, 'r', "lectura", tipoUsuario);
	pedirPermiso(mascara, 'w', "escritura", tipoUsuario);
	pedirPermiso(mascara, 'x', "ejecución", tipoUsuario);

}

void setValue(int *mascara, char permiso, char op) {

	switch(op) {
		case 's':
			setPermiso(mascara, permiso, SET);
			break;
		case 'n':
			setPermiso(mascara, permiso, UNSET);
			break;
	}

}

void pedirPermiso(int *mascara, char permiso, char *tipoPermiso, char *tipoUsuario) {

	char p;

	printf("Permiso %s para %s? [s/n] ", tipoPermiso, tipoUsuario);
	scanf("%c", &p);

	setValue(mascara, permiso, p);

	getchar();

}